function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	} else {
		x.innerHTML = "Geolocation is not supported by this browser.";
	}
}
function showPosition(position) {
	printMap(position.coords.latitude, position.coords.longitude);
}
function printMap(lat, lon) {
	var mapOptions = {
		center: new google.maps.LatLng(lat, lon),
		zoom: 17,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
	addMarker(map, lat, lon);
}
function addMarker(map, lati, lon) {
	var myLatLng = {lat: lati, lng: lon};
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title: 'Your position'
	});
}
