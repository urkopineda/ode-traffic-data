function loadXML(max) {
	var request = new XMLHttpRequest();
	request.open('GET', 'http://pruebas.s18435144.onlinehome-server.info/data/data.xml', true);
	request.onreadystatechange = function() {
		if ((request.readyState == 4) && (request.status == 200)) {
			var xmlDoc = request.responseXML;
			var xmlRows = xmlDoc.documentElement.getElementsByTagName('incidenciaGeolocalizada');
			for (var i = 0; i < xmlRows.length; i++) {
				var xmlRow = xmlRows[i];
				var row = document.getElementById('table').insertRow(i + 1);
				for (var j = 0; j < max; j++) {
					switch (j) {
						case 0: var cell1 = row.insertCell(j);
							cell1.innerHTML = xmlRow.getElementsByTagName("tipo")[0].innerHTML;
							break;
						case 1: var cell2 = row.insertCell(j);
							cell2.innerHTML = xmlRow.getElementsByTagName("autonomia")[0].innerHTML;
							break;
						case 2: var cell3 = row.insertCell(j);
							cell3.innerHTML = xmlRow.getElementsByTagName("provincia")[0].innerHTML;
							break;
						case 3: var cell4 = row.insertCell(j);
							cell4.innerHTML = xmlRow.getElementsByTagName("poblacion")[0].innerHTML;
							break;
						case 4: var cell5 = row.insertCell(j);
							cell5.innerHTML = xmlRow.getElementsByTagName("causa")[0].innerHTML;
							break;
						case 5: var cell6 = row.insertCell(j);
							cell6.innerHTML = xmlRow.getElementsByTagName("fechahora_ini")[0].innerHTML;
							break;
						case 6: var cell7 = row.insertCell(j);
							cell7.innerHTML = xmlRow.getElementsByTagName("nivel")[0].innerHTML;
							break;
						case 7: var cell8 = row.insertCell(j);
							cell8.innerHTML = xmlRow.getElementsByTagName("carretera")[0].innerHTML;
							break;
						case 8: var cell9 = row.insertCell(j);
							cell9.innerHTML = xmlRow.getElementsByTagName("pk_inicial")[0].innerHTML;
							break;
						case 9: var cell10 = row.insertCell(j);
							cell10.innerHTML = xmlRow.getElementsByTagName("pk_final")[0].innerHTML;
							break;
						case 10: var cell11 = row.insertCell(j);
							cell11.innerHTML = xmlRow.getElementsByTagName("sentido")[0].innerHTML;
							break;
						case 11: var cell12 = row.insertCell(j);
							cell12.innerHTML = xmlRow.getElementsByTagName("longitud")[0].innerHTML;
							break;
						case 12: var cell13 = row.insertCell(j);
							cell13.innerHTML = xmlRow.getElementsByTagName("latitud")[0].innerHTML;
							break;
						case 13: var cell14 = row.insertCell(j);
							cell14.innerHTML = "<button onclick='printMap("+cell13.innerHTML+", "+cell12.innerHTML+");'>Load map</button>";
							break;
					}
				}
			}
		}
	}
	request.send(null);
}
